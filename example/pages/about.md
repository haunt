title: About
index: true
---
Scheme is a cool programming language.

Guile is a cool Scheme implementation.

Haunt is a cool static site generator written in Guile.

Enough said.
